/* Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "dsi-panel-nt35532-1080p-skuk-video.dtsi"
#include "msm8939-audio-internal_codec-phoenix.dtsi"
#include "msm8939-pinctrl-phoenix.dtsi"
#include "msm8939-camera-sensor-phoenix.dtsi"
#include "msm-pm8916-phoenix.dtsi"

/ {
	aliases {
		/delete-property/ spi0;
		i2c3 = &i2c_3;
	};

	memory {
		pstore_reserve_mem: pstore_reserve_region@0 {
			linux,reserve-contiguous-region;
			linux,reserve-region;
			linux,remove-completely;
			reg = <0x0 0x9ff00000 0x0 0x00100000>;
			label = "pstore_reserve_mem";
		};
	};
};

&blsp1_uart1 {
	status = "disabled";
};

&blsp1_uart2 {
	status = "ok";
	pinctrl-names = "default";
	pinctrl-0 = <&uart_console_sleep>;
};

&soc {
	/delete-node/ spi@78b7000;

	gpio_keys {
		compatible = "gpio-keys";
		input-name = "gpio-keys";
		pinctrl-names = "tlmm_gpio_key_active","tlmm_gpio_key_suspend";
		pinctrl-0 = <&gpio_key_active>;
		pinctrl-1 = <&gpio_key_suspend>;

		vol_up {
			label = "volume_up";
			gpios = <&msm_gpio 107 0x1>;
			linux,input-type = <1>;
			linux,code = <115>;
			gpio-key,wakeup;
			debounce-interval = <15>;
		};
	};

	gpio-leds {
		compatible = "gpio-leds";
		status = "okay";
		pinctrl-names = "default";
		pinctrl-0 = <&gpio_led_off>;

		green {
			gpios = <&msm_gpio 119 2>;
			label = "green";
			linux,default-trigger = "none";
			default-state = "off";
			retain-state-suspended;
		};

		blue {
			gpios = <&msm_gpio 8 2>;
			label = "blue";
			linux,default-trigger = "none";
			default-state = "off";
		};
	};

	i2c_3: i2c@78b7000 {
		compatible = "qcom,i2c-msm-v2";
		#address-cells = <1>;
		#size-cells = <0>;
		reg-names = "qup_phys_addr", "bam_phys_addr";
		reg = <0x78b7000 0x600>,
		      <0x7884000 0x23000>;
		interrupt-names = "qup_irq", "bam_irq";
		interrupts = <0 97 0>, <0 238 0>;
		clocks = <&clock_gcc clk_gcc_blsp1_ahb_clk>,
			 <&clock_gcc clk_gcc_blsp1_qup3_i2c_apps_clk>;
		clock-names = "iface_clk", "core_clk";
		qcom,clk-freq-out = <100000>;
		qcom,clk-freq-in  = <19200000>;
		pinctrl-names = "i2c_active", "i2c_sleep";
		pinctrl-0 = <&i2c_3_active>;
		pinctrl-1 = <&i2c_3_sleep>;
		qcom,noise-rjct-scl = <0>;
		qcom,noise-rjct-sda = <0>;
		qcom,bam-pipe-idx-cons = <8>;
		qcom,bam-pipe-idx-prod = <9>;
		qcom,master-id = <86>;
	};

	i2c@78ba000 {
		goodix@5d {
			compatible = "goodix,gt9xx";
			reg = <0x5d>;
			interrupt-parent = <&msm_gpio>;
			interrupts = <13 0x2008>;
			vdd-supply = <&pm8916_l17>;
			vcc_i2c-supply = <&pm8916_l6>;
			pinctrl-names = "pmx_ts_active","pmx_ts_suspend";
			pinctrl-0 = <&ts_int_active &ts_reset_active>;
			pinctrl-1 = <&ts_int_suspend &ts_reset_suspend>;
			goodix,name = "gt9xx";
			goodix,family-id = <0x14>;
			goodix,reset-gpio = <&msm_gpio 12 0x0>;
			goodix,irq-gpio = <&msm_gpio 13 0x2008>;
			goodix,display-coords = <0 0 1080 1920>;
			goodix,panel-coords = <0 0 1080 2098>;
			goodix,no-force-update;
			goodix,i2c-pull-up;
			goodix,group-id = <1>;
			goodix,fw-upgrade-id1 = <0x11>;
			goodix,fw-upgrade-id2 = <0x11>;
			goodix,fw-delay-readid-ms = <10>;
			goodix,fw-delay-era-flsh-ms = <2000>;
			goodix,fw-auto-cal;
			goodix,ignore-id-check;
	    };
	};

	hall_sensor {
		compatible = "qcom,hall";
		input-name = "hall_sensor";

		pinctrl-names = "s5712_default";
		pinctrl-0 = <&hall_sensor_default>;
		interrupt-parent = <&msm_gpio>;
		interrupts = <117 0x2003>;
		hall,int-gpio = <&msm_gpio 117 0x2003>;
		hall,input-type = <1>;
		hall,active-lvl = <0>;
		hall,active_code = <148>;
		hall,inactive_code = <149>;
		hall,debounce-interval = <20>;
	};

	usb_otg: usb@78d9000 {
		qcom,hsusb-otg-phy-init-seq =
			<0x74 0x80 0x6D 0x81 0x3F 0x82 0x33 0x83 0xffffffff>;
	};

	ramoops {
		compatible = "ramoops";
		status = "ok";

		android,ramoops-buffer-start = <0x9ff00000>;
		android,ramoops-buffer-size = <0x100000>;
		android,ramoops-console-size = <0x40000>;
		android,ramoops-pmsg-size = <0x40000>;
		android,ramoops-record-size = <0x20000>;
		android,ramoops-dump-oops = <0x1>;
	};
};

&spmi_bus {
	qcom,pm8916@1 {
		qcom,vibrator@c000 {
			status = "okay";
			qcom,vib-timeout-ms = <15000>;
			qcom,vib-vtg-level-mV = <2700>;
		};
	};

	qcom,pm8916@0 {
		qcom,leds@c100 {
			status = "okay";
			qcom,led_gpio_2 {
				label = "gpio";
				linux,name = "red";
				linux,default-trigger = "none";
				qcom,default-state = "off";
				qcom,max-current = <5>;
				qcom,current-setting = <5>;
				qcom,id = <8>;
				qcom,mode = "manual";
				qcom,source-sel = <1>;
				qcom,mode-ctrl = <0x60>;
			};
		};
	};
};

&pm8916_mpps {
	mpp@a300 {
		qcom,mode = <1>;
		qcom,invert = <0>;
		qcom,src-sel = <4>;
		qcom,vin-sel = <0>;
		qcom,master-en = <0>;
	};
};

&i2c_0 {
	bosch@18 {
		compatible = "bosch,bma2x2";
		reg = <0x18>;
		pinctrl-names = "bma_default","bma_sleep";
		pinctrl-0 = <&bma2x2_int1_default &bma2x2_int2_default>;
		pinctrl-1 = <&bma2x2_int1_sleep &bma2x2_int2_sleep>;
		interrupt-parent = <&msm_gpio>;
		interrupts = <112 0x2002>;
		vdd-supply = <&pm8916_l17>;
		vio-supply = <&pm8916_l6>;
		bosch,init-interval = <200>;
		bosch,place = <6>;
		bosch,gpio-int1 = <&msm_gpio 112 0x2002>;
		bosch,gpio-int2 = <&msm_gpio 114 0x2002>;
	};

	tr@23 {
		compatible = "ltr,ltr559"; 
		reg = <0x23>; 
		pinctrl-names = "default","sleep"; 
		pinctrl-0 = <&ltr_default>; 
		pinctrl-1 = <&ltr_sleep>; 
		interrupt-parent = <&msm_gpio>; 
		interrupts = <115 0x2002>; 
		vdd-supply = <&pm8916_l17>; 
		vio-supply = <&pm8916_l6>; 
		ltr,irq-gpio = <&msm_gpio 115 0x2002>; 
		ltr,ps-threshold = <650>; 
		ltr,ps-hysteresis-threshold = <500>; 
		ltr,als-polling-time = <200>; 
	}; 

	memsic@30 {
		compatible = "memsic,mmc3524x";
		reg = <0x30>;
		vdd-supply = <&pm8916_l17>;
		vio-supply = <&pm8916_l6>;
		memsic,dir = "obverse-x-axis-forward";
		memsic,auto-report;
	};

	tps65132@3e {
		compatible = "ti,tps65132";
		reg = <0x3e>;
		i2c-pwr-supply = <&pm8916_l6>;
		ti,en-gpio-lpm;
		pinctrl-names = "default";
		pinctrl-0 = <&tps65132_en_default>;

		regulators {
			tps65132_pos: pos-boost {
				regulator-name = "tps65132-pos";
				regulator-min-microvolt = <5800000>;
				regulator-max-microvolt = <5800000>;
				ti,discharge-enable;
				ti,enable-time = <800>;
				ti,current-limit = <200000>;
				ti,en-gpio = <&msm_gpio 97 0>;
			};

			tps65132_neg: neg-boost {
				regulator-name = "tps65132-neg";
				regulator-min-microvolt = <5800000>;
				regulator-max-microvolt = <5800000>;
				ti,discharge-enable;
				ti,enable-time = <800>;
				ti,current-limit = <40000>;
				ti,en-gpio = <&msm_gpio 98 0>;
			};
		};
	};
};

&android_usb {
	qcom,android-usb-cdrom;
};

&apc_vreg_corner {
	vdd-apc-supply = <&fan53555>;
	qcom,cpr-step-quotient = <26>;
	qcom,cpr-apc-volt-step = <12500>;
	qcom,cpr-up-threshold = <2>;
};

&mdss_dsi0 {
	qcom,dsi-pref-prim-pan = <&dsi_nt35532_1080p_skuk_video>;
	pinctrl-names = "mdss_default", "mdss_sleep";
	pinctrl-0 = <&mdss_dsi_active>;
	pinctrl-1 = <&mdss_dsi_suspend>;

	qcom,platform-reset-gpio = <&msm_gpio 25 0>;
	qcom,platform-enable-gpio = <&msm_gpio 2 0>;

	/delete-property/ vdda-supply;
	/delete-property/ vdd-supply;

	vsp-supply = <&tps65132_pos>;
	vsn-supply = <&tps65132_neg>;

	qcom,ctrl-supply-entries {
		/delete-node/ qcom,ctrl-supply-entry@0;
	};

	qcom,panel-supply-entries {
		/delete-node/ qcom,panel-supply-entry@0;

		qcom,panel-supply-entry@2 {
			reg = <2>;
			qcom,supply-name = "vsp";
			qcom,supply-min-voltage = <5800000>;
			qcom,supply-max-voltage = <5800000>;
			qcom,supply-enable-load = <200>;
			qcom,supply-disable-load = <0>;
		};

		qcom,panel-supply-entry@3 {
			reg = <3>;
			qcom,supply-name = "vsn";
			qcom,supply-min-voltage = <5800000>;
			qcom,supply-max-voltage = <5800000>;
			qcom,supply-enable-load = <40>;
			qcom,supply-disable-load = <0>;
		};
	};
};

&mdss_mdp {
	qcom,mdss-pref-prim-intf = "dsi";
};

&pmx_mdss {
	qcom,num-grp-pins = <1>;
	qcom,pins = <&gp 25>;
};

&qcom_tzlog {
	status = "okay";
};

&qcom_rng {
	status = "okay";
};

&qcom_crypto {
	status = "okay";
};

&qcom_cedev {
	status = "okay";
};

&qcom_seecom {
	status = "okay";
};

&sdhc_1 {
	vdd-supply = <&pm8916_l8>;
	qcom,vdd-voltage-level = <2900000 2900000>;
	qcom,vdd-current-level = <200 400000>;

	vdd-io-supply = <&pm8916_l5>;
	qcom,vdd-io-always-on;
	qcom,vdd-io-lpm-sup;
	qcom,vdd-io-voltage-level = <1800000 1800000>;
	qcom,vdd-io-current-level = <200 60000>;

	pinctrl-names = "active", "sleep";
	pinctrl-0 = <&sdc1_clk_on &sdc1_cmd_on &sdc1_data_on>;
	pinctrl-1 = <&sdc1_clk_off &sdc1_cmd_off &sdc1_data_off>;

	qcom,nonremovable;

	status = "ok";
};

&sdc2_cd_off {
	/delete-property/ bias-disable;
	bias-pull-up;
};

&sdhc_2 {
	vdd-supply = <&pm8916_l11>;
	qcom,vdd-voltage-level = <2800000 2950000>;
	qcom,vdd-current-level = <15000 400000>;

	vdd-io-supply = <&pm8916_l12>;
	qcom,vdd-io-voltage-level = <1800000 2950000>;
	qcom,vdd-io-current-level = <200 50000>;

	pinctrl-names = "active", "sleep";
	pinctrl-0 = <&sdc2_clk_on &sdc2_cmd_on &sdc2_data_on &sdc2_cd_on>;
	pinctrl-1 = <&sdc2_clk_off &sdc2_cmd_off &sdc2_data_off &sdc2_cd_off>;

	#address-cells = <0>;
	interrupt-parent = <&sdhc_2>;
	interrupts = <0 1>;
	#interrupt-cells = <1>;
	interrupt-map-mask = <0xffffffff>;
	interrupt-map = <0 &intc 0 125 0
		1 &intc 0 221 0>;
	interrupt-names = "hc_irq", "pwr_irq";
	cd-gpios = <&msm_gpio 38 0x1>;

	status = "ok";
};

&usb_otg {
	qcom,hsusb-otg-mode = <3>;
	qcom,usbid-gpio = <&msm_gpio 110 0>;
	pinctrl-names = "default";
	pinctrl-0 = <&usbid_default>;
	vbus_otg-supply = <&smb358_otg_supply>;
};
